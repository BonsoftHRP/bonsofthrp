Bonsoft-HRP is the enterprise web application project for Human Resources Planing.
----------------------------

#Overview

#Tool dependencies

 - Programming language: Java
 - Framework: Spring Boot, Spring MVC
 - Build automating tool: Gradle/Maven
 - Testing tools: JUnit, Spock
 - Code quality: Checkstyle
 - Code coverage: Jacoco
 - Logging: SLF4j, Logback

#How to build
For snapshot version just type ./gradlew
For release version, example ./gradlew -Pmajor=1 -Pminor=0 -Ppatch=0 -Pbranch=master
Or customize build.sh and launch it.

